$(window).load(function(){
   var cars = $('.carusel');
   for (var j=0;j<cars.length;j++) {
      preparePage(cars.eq(j).closest('.menu-slider').find('.overflow'));
      prepareCar(cars.eq(j));
   }
   
   resizer();
});

$(document).ready(function() {
   resizer();
   
   
   $(window).resize(function(){
      resizer();
   });

   try {
      $(window).bind('orientationchange', function () {
         resizer();
      });
   }catch(e){}



   $('input, textarea').focus(function(){
         var def = $(this).attr("defvalue")||'',
            val = $(this).val();
         if (def===val){
            $(this).val('');

         }
         $(this).addClass('with-val');
      });

   $('input, textarea').blur(function(){
      var def = $(this).attr("defvalue")||'',
         val = $(this).val();
      if (def===val || !val){
         $(this).val(def);
         $(this).removeClass('with-val');

      }
   });

   $('.phone-in').keydown(function(ev){
      if (ev.keyCode!==9 && (ev.keyCode<48 || ev.keyCode>57))
         if (ev.keyCode===8 || ev.keyCode===46 || ev.keyCode===37 || ev.keyCode===39 || ev.keyCode===36 || ev.keyCode===35 || ev.keyCode===107 || ev.keyCode===39){} else
            return false;
   });



   $('form').submit(function(ev){
      var d = $("<div class='message'>Ваше сообщение <br> отправлено администратору</div>");
      $('body').append(d);

      setTimeout(function(){d.addClass('disable');}, 1000);
      setTimeout(function(){d.remove();}, 2000);
      return false;
   })


   $('.arr-left').click(function(){
      slide(1, $(this));
   });

   $('.arr-right').click(function(){
      slide(-1, $(this));
   });

   $('.one-sw').click(function(){
      $('.one-sw').removeClass('active');

      $(this).addClass('active');

      $('.gorizontal-slider').removeClass('active');
      $('.menu-slider').removeClass('active');
      $('.gorizontal-slider[slider="'+$(this).attr('slider')+'"]').addClass('active');
      $('.menu-slider[slider="'+$(this).attr('slider')+'"]').addClass('active');
   });


   $('.text-in-double:not(.active) .for-scroll').niceScroll({cursorcolor:"#b6b5b5", autohidemode:false, background: "#cac9c9"});

   $('.text-in-double.active .for-scroll').niceScroll({cursorcolor:"#b6b5b5", autohidemode:false, background: "#cac9c9"});

   $('.double-vis .button').click(function(ev){
      $(this).closest('.double-vis').find('.text-in-double').addClass("active");
      var obj = $(this).closest('.double-vis').find('.for-scroll').getNiceScroll().eq(0);
      obj.show();
//      if (!obj.isinited) {
         obj.onResize();
//         obj.isinited = 1;
//      }
      ev.stopImmediatePropagation();
   });

   $('.gold-download').click(function(){
      $(this).next().trigger('click');
   });

   $('.button').click(function(ev){
      var f = $(this).closest("form");
      if (f.length>0)
         f.submit();
      else{
         $('.form').show();
      }
      ev.stopImmediatePropagation();
   });

   $('.form').click(function(ev){
      ev.stopImmediatePropagation();
   });

   $('body, .close').click(function(){
      $('.form').hide();
   });

   $('.close-but').click(function(){
      $(this).closest('.double-vis').find('.only-click').removeClass("active");
      $(this).closest('.double-vis').find('.for-scroll').getNiceScroll().eq(0).hide();
   });

   createNavigate();
   changeSel();
   $(window).scroll(changeSel);

   $('.menu').click(function(ev){
      ev.stopImmediatePropagation();
      $('.inner-menu').show();
   });
   $('.gold-menu').click(function(ev){
      ev.stopImmediatePropagation();
      $('.inner-menu').show();
   });
   $('body').click(function(){
      $('.inner-menu').hide();
   });

   var timerId;
   $('.switch').on('click', '.one-el', function(el, isAuto){
      if ( ! isAuto ) {
         clearInterval(timerId);
      }
      var i = $(this).attr("i");
      if (i!==undefined){
         var carusel = $(this).closest('.carusel');

         carusel.find('.one-img').removeClass('active');
         carusel.find('.one-img').eq(parseInt(i,10)).addClass('active');
         carusel.find('.one-el').removeClass('active');

         prepareCar(carusel);
      } else {
         var rs = $(this).closest('.r-slider');
         if (rs.length===0){
            rs = $(this).closest('.promo');
            rs.css({"background-image": 'url(' + $(this).attr("ph") + ')'});
         } else {
            rs.find('.r-slide').css({"background-image": 'url(' + $(this).attr("ph") + ')'});
            rs.find('.r-slide').toggleClass('switchgo');
         }

         rs.find('.one-el').removeClass('active');
      }

      $(this).addClass('active');
   });

   if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      timerId = setInterval(function () {
         $('.switch').each( function () {
            var $this = $(this),
                $next = $this.find('.active').next();

            if ( $next.length ) {
               $next.trigger('click', [true]);
            } else {
               $this.children().first().trigger('click', [true]);
            }
         } );
      }, 7000);
   }

   $('.long-arr-right').click(function(){
      if ($(this).closest('.carusel').length===0)
         changeActive(1, $(this));
      else
         changeActiveCarusel(1, $(this));
   });

   $('.long-arr-left').click(function(){
      if ($(this).closest('.carusel').length===0)
         changeActive(0, $(this));
      else
         changeActiveCarusel(0, $(this));
   });

   $('.arr2 .arr-left').click(function(){
      goslide(-1, $(this));
   });

   $('.arr2 .arr-right').click(function(){
      goslide(1, $(this));
   });

   var cars = $('.carusel');
   for (var j=0;j<cars.length;j++) {
      preparePage(cars.eq(j).closest('.menu-slider').find('.overflow'));
      prepareCar(cars.eq(j));
   }

   $('.gorizontal-slider .arr-left').hide();

   $('.one-sl').click(function(){
      location.href = $(this).find('a').attr('href');
   });

   initSwipe();
});


function initSwipe(){
   var initialPoint;
   var finalPoint;
   $('.double-vis').on('touchstart', function(event) {
      //event.preventDefault();
      //event.stopPropagation();
      initialPoint=event.changedTouches[0];
      console.log(initialPoint)
   });
   $('.double-vis').on('touchend', function(event) {

      finalPoint=event.changedTouches[0];
      console.log(finalPoint);
      var xAbs = Math.abs(initialPoint.pageX - finalPoint.pageX);
      var yAbs = Math.abs(initialPoint.pageY - finalPoint.pageY);
      if (xAbs > 20 || yAbs > 20) {
         if (xAbs > yAbs) {
            if (finalPoint.pageX < initialPoint.pageX){
               console.log('left');
               $(this).find('.arr-left').trigger('click');
            }
            else{
               console.log('right');
               $(this).find('.arr-right').trigger('click');
            }

            //event.preventDefault();
            //event.stopPropagation();
         }
         else {
            if (finalPoint.pageY < initialPoint.pageY){
               //СВАЙП ВВЕРХ
            }
            else{
               //СВАЙП ВНИЗ
            }
         }
      }
   });
}

function changeActiveCarusel(to, el){
   var car = el.closest('.carusel'),
      ol = car.find('.one-el'),
      ac = car.find('.one-el.active');

   if (to===1){
      ac = ac.next();
      if (ac.length===0){
         ac = ol.eq(0);
      }
   } else {
      ac = ac.prev();
      if (ac.length===0){
         ac = ol.eq(ol.length-1);
      }
   }

   ac.trigger('click');

}

function changeActive(to, el){
   var ov = el.closest('.overflow'),
      tho = el.closest('.one-page'),
      onp = ov.find('.one-page');

   onp.removeClass('active');

   if (to===1){
      tho.next().addClass('active');
   } else {
      tho.prev().addClass('active');
   }

   preparePage(ov);
   prepareCar(ov.next());
}

function preparePage(car){
   var im = car.find('.one-page'),
      isActive = 0;

   im.removeClass('before');
   im.removeClass('after');

   for(var i=0;i<im.length;i++){
      var imi = im.eq(i);

      if (imi.hasClass('active')){
         isActive=1;
      } else if (isActive===0){
         imi.addClass('before');
      } else if (isActive===1){
         imi.addClass('after');
      }
   }


}

function prepareCar(car){
   var im = car.find('.one-img'),
      maxh = 0,
      isActive = 0;

   im.removeClass('before');
   im.removeClass('after');

   for(var i=0;i<im.length;i++){
      var imi = im.eq(i),
         img = imi.find('img'),
         w = img.width(),
         h = img.height();

      if (h>maxh)
         maxh = h;

      imi.css({"margin-left": "-" +w/2+"px",
         "margin-top": "-" +h/2+"px"});

      if (imi.hasClass('active')){
         isActive=1;
         (function(imi){
            setTimeout(function(){
               imi.css({"z-index":50});
            }, 200);
         })(imi);
      } else if (isActive===0){
         imi.addClass('before');
         (function(imi,i){
            setTimeout(function(){
               imi.css({"z-index":3+i});
            }, 200);
         })(imi,i);
      } else if (isActive===1){
         imi.addClass('after');
         (function(imi,i){
            setTimeout(function(){
               imi.css({"z-index":25-i});
            }, 200);
         })(imi,i);
      }


   }
   car.css({"height":maxh+"px"});

   maxh+=190;
   var ms = car.closest('.menu-slider'),
      ov = ms.find('.overflow'),
      op = ov.find('.one-page.active'),
      ovh = op.height();

   ov.css({"height":(ovh)+"px"});
   ms.css({"height":(ovh+maxh)+"px"})

}

function goslide(to, el){
   var arr = el.closest('.arr2'),
      cur = parseInt(arr.attr('current'),10),
      imgs = JSON.parse(arr.attr('imgs'));

   if (!cur){
      cur = 0;
   }

   cur += to;

   if (cur<0)
      cur=imgs.length-1;
   if (cur>=imgs.length){
      cur=0;
   }
   var temp = $("<img src='"+imgs[cur]+"' class='newimg "+(to>0?"fromleft":"fromright")+"'/>");
   arr.prev('.foot-vis').before( temp );
   arr.attr('current', cur);
   setTimeout(function(){
      temp.removeClass("fromleft");
      temp.removeClass("fromright");
   },1);

}

function createNavigate(){
   var div = $('.navigate'),
      br = $('.border-rights');

   if (br.length===0)
      return;
   var w = br.width();

   div.find('.zoom').css({
      width:"610px"
   });
   div.find('.zoom')[0].innerHTML = br[0].innerHTML;

   div.css('height', 20+div.find('.zoom').height()/10+"px");
}

function changeSel(){
   var div = $('.navigate'),
      br = $('.border-rights'),
      sel = $('.sel');

   if (sel.length===0)
      return;

   var top = br.offset().top,
      scrollY = window.scrollY,
      zero = scrollY-top;

   if (zero<0) zero=0;

   sel.css({'top':(zero/10)+"px"});

   if (scrollY>top-27){
      div.css({"position":"fixed",
         left: div.offset().left+"px",
         right: "auto"});
   } else {

      div.css({"position":"absolute",
            left: "auto",
            right: "10px"});
   }
}

function slide(k, el){
   var ov = el.closest('.overflow'),
      sl = ov.find('.all-slides'),
      count = sl.find('.one-sl').length,
      ml = parseInt(sl.attr("marginleft"),10);
   if (!ml)
      ml = 0;

   ml += k;

      if (ml > 0) {
         return;
      }

      if (ml >= 0) {
         ov.find('.arr-left').hide();
      } else {
         ov.find('.arr-left').show();
      }

      if (ml < -count + 2) {
         return;
      }
      if (ml <= -count + 2) {

         ov.find('.arr-right').hide();
      } else {
         ov.find('.arr-right').show();
      }

   sl.attr("marginleft", ml);
   sl.css({"margin-left":ml*468+"px"});
}

function resizer(){
   setTimeout(function(){
      
      var w = $('body').width(),
         h = $(document).height();

      var b100 = $('.promo');
      for (var i=0;i<b100.length;i++){
         var hr = b100.eq(i).height(),
            padding = ($(window).height()-hr)/2;
            
         if (padding>0){
            b100.eq(i).css({"padding-top":padding+"px", "padding-bottom":padding+"px"});
         }
      }


      if (w<769){

         $('body').removeClass('nolmalw');
         if (w<481) {
            $('body').removeClass('w1024');
            $('body').addClass('w480');
         } else {
            $('body').addClass('w1024');
            $('body').removeClass('w480');
         }
      } else {
         $('body').removeClass('w1024');
         $('body').removeClass('w480');
         $('body').addClass('nolmalw');
      }
   }, 1);
}
